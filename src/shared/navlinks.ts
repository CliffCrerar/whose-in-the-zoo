import { iNavLink } from '../components/index.d';

export default [
    { key: 'nav-0', caption: 'Home', href: null },
    { key: 'nav-1', caption: 'Animals', href: 'animals' },
    { key: 'nav-2', caption: 'Zookeepers', href: 'zookeepers' },
    { key: 'nav-3', caption: 'Habitats', href: 'habitats' }
] as iNavLink[];

