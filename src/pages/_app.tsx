import App, { AppProps } from 'next/app'
import '../scss/css-rad.scss';
import '../scss/global.scss';
import Layout from '../layout/index';
import AppHead from '../components/head';
import { Fragment } from 'react';


function MyApp({ Component, pageProps }: AppProps) {
    return (
        <Fragment>
            <AppHead title={'TEMP'} />
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </Fragment>
    )
}

export default MyApp