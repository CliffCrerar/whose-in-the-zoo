/** 
 * Animals
 */

import { Fragment, useState, useEffect } from "react";
import CrudHeader from '../components/header';
import { CreateAnimal, RetrieveAnimal, UpdateAnimal, DeleteAnimal } from "../components/animal-crud";



const Animals = (props) => {
  console.log('props: ', props);

  // const [form,setForm] = useState(0)
  useEffect(() => {
    getRefData();
  })

  function getRefData() {
    // const refData = fetch('/test.ts').then(res => res.text());
      // refData.then(body=> console.log(body))
    
    // console.log('res: ', res);
    
  }

  function clickedTarget(param: string) {
    // setForm(param);
  }

  return (
    <Fragment>
      <CrudHeader />
      <main className="container">
        <CreateAnimal />
        {/* <RetrieveAnimal /> */}
        {/* <UpdateAnimal /> */}
        {/* <DeleteAnimal /> */}
      </main>
    </Fragment>
  )
}

// export async function getStaticProps  () {

//   async function fetchData(){  
//     const res = fetch('/animal').then(res=>console.log('data: ', res))
//     return {};
//   }

//   return {
//     props: {p:fetchData()}  
//   }
// }

export default Animals;