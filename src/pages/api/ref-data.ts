import PG, { Query } from 'pg';
import { config } from 'dotenv';
config();

const query = 'select * from habitats;select * from animals; select * from category; select * from zookeepers;';

export default (req, res) => {
    console.log('RUN DB TEST API API');
    res.setHeader('Content-Type', 'application/json')
    new PG.Pool().connect()
        .then(client => {
            client.query(query)
                .then(dbRes => res.status(200).send(dbRes))
                .catch(dbRes => res.status(501).send(dbRes))
        })
        .catch(connectErr => res.status(503).send(connectErr));
}