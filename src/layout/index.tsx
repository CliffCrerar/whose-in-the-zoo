/**
 * Layout
 */

import Links from '../components/nav';
import { Props } from 'react';
import {Fragment} from 'react';

function Layout(props: Props<any>) {

    const logoUrl = 'https://10ark.com/wp-content/uploads/2016/07/Logo-rework-2.svg'

    return (
        <Fragment>

            <nav>

                <div className="nav-logo position-relative">

                    <img height='40px' src={logoUrl} alt="10ARK digital logo"/>

                </div>
                
                <Links/>

            </nav>



            {props.children}

            <footer>

                10ARK Digital : Candidate Assesment Demo

            </footer>

        </Fragment>
    )
}

export default Layout;