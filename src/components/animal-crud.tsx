import { Props, useEffect, FormEvent } from "react";
import { Animal } from '../models/zoo.models';

function CreateAnimal() {

    useEffect(()=>{
        // document.addEventListener()
        // populateForm();
    })



    function submitForm(event: FormEvent<HTMLFormElement>){
        
        event.persist();
        event.preventDefault();
        
        console.log('event: ', event);
    }

    return (
        <form onSubmit={submitForm}>
            <h1>Create</h1>
            <div>
                <label>
                    Name:
                    <input className="w-100"  name="animalname" type="text"></input>
                </label>

            </div>
            <div>

                <label>
                    Habitat:
                    <select className="w-100"  name="habitat"></select>
                </label>

            </div>
            <div>

                <label>
                    Category:
                    <select className="w-100" name="category"></select>
                </label>
            </div>
            <div>

            <label>
                Zookeeper:
                    <select className="w-100"  name="category"></select>
            </label>

            <button type="submit">Submit</button>
            </div>
        </form >
    )
}


function RetrieveAnimal(props: Props<Animal>) {

    return (
        <div>
            <h1>Retrieve</h1>
        </div>
    )
}


function UpdateAnimal(props: Props<Animal>) {


    return (<form>
        <h1>Update</h1>
    </form>
    )
}


function DeleteAnimal() {
    return (<form>
        <h1>Delete</h1>
    </form>
    )
}

export { CreateAnimal, RetrieveAnimal, UpdateAnimal, DeleteAnimal }