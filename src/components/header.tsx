import { useEffect, MouseEvent } from "react";


function CrudHeader(props) {
    useEffect(() => {
        const btnContainer = document.getElementById('hdrBtnContainer');
        console.log('btnContainer: ', btnContainer);
        btnContainer.addEventListener('click', (ev: Event) => {
            const evTarget = ev.target as HTMLElement;
            console.log(ev);
            props.selectedForm(evTarget.innerText);
        })
    })
    return (
        <header className="container">
            <div id="hdrBtnContainer" className="crud-header w-100 d-flex flex-row justify-content-around">
                <button>Create</button>
                <button>Retrieve</button>
                <button>Update</button>
                <button>Delete</button>
            </div>
            <hr />
        </header>
    )
}

export default CrudHeader;