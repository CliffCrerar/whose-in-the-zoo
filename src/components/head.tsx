/** 
 * Head
 */
import Head from 'next/head';
import { Props, ReactElement } from 'react';

interface HeadProps{
    title
}

export default (props: HeadProps): ReactElement => {
    console.log('HEAD: ', props);
    return (
        <Head>
            <title>{props.title}</title>
            <link rel="icon" href="/favicon.ico" />
            {/* <link rel="stylesheet" href={styles} /> */}
        </Head>
    )
}
