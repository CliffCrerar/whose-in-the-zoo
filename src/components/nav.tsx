
/**
 * Nav
 */

import Link from 'next/link';
import navLinks from '../shared/navlinks';
import { iNavLink } from './index.d';
import { useEffect } from 'react';
import Navlinks from '../shared/navlinks';

export default (props) => {

    const Links = navLinks.map((link: iNavLink, idx: number) => {
        // console.log('link: ', link);
        return (
            <li key={link.key} className={`pl-4 pr-4 ${idx===0 && 'active'}`}>
                <Link href={link.href===null ? '/' : link.href}>
                    <a>{link.caption}</a>
                </Link>
            </li>
        )
    })

    useEffect(()=>{
        // Highlight effect
        const navList = document.getElementById('nav-list');
        navList.addEventListener('click',(ev:MouseEvent)=>{
            const eventTarget = ev.target as HTMLElement;
            for(let i =0;i<navList.children.length;i++){
                navList.children[i].classList.remove('active');
            }
            eventTarget.parentElement.classList.add('active')
        })
    })

    return (
        <ul id="nav-list" className="d-flex">
            {Links}
        </ul>
    )
}

// export async function getStaticProps(context){
//     return {
//         props:{
//             navLinks:[
//                 { caption: 'Home', href: null },
//                 { caption: 'Zookeepers', href: 'zookeepers' },
//                 { caption: 'Animals', href: 'animals' },
//                 { caption: 'Habitats', href: 'habitats' }
//             ]
//         }
//     }
// }