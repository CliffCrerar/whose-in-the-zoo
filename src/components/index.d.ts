
export interface iNavLink{
    caption: string;
    href: string,
    key: string
}

declare module 'componentsObjects'{
    NavLink: NavLink;

    export {NavLink};
}