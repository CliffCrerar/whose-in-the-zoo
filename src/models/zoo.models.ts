export class Animal {
    public static id: number;
    constructor(
        public name: string,
        public zooKeeper: ZooKeeper,
        public category: Category,
        public habitat: Habitat
    ){
        Animal.id = 1001;
    }
}


export class ZooKeeper{
    constructor(
        public zookeeperid: number,
        public firstName: string,
        public lastname: string
    ){}
}

export class Habitat{
    constructor(
        public habitatId: number,
        public country: string,
        public environment: string
    ){}
}

export class Category{
    constructor(
        public categoryid: number,
        public categoryname: string
        ){}
}