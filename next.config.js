// next.config.js
require('dotenv').config();
const sass = require('@zeit/next-sass');
const withPlugins = require('next-compose-plugins');
const withSourceMaps = require('@zeit/next-source-maps');

process.env['NODE_PATH'] = ['/src', 'public', '/sr/layout'].join(require('os').platform() === 'win32' ? ';' : ':');

const plugins = [
  [sass], 
  [withSourceMaps]
];

const config = {
  // target: 'server',
  // Expose web pack config
  webpack: (config, options) => {

    // modify the `config` here

    return config;
  },
}


module.exports = withPlugins (plugins,config);